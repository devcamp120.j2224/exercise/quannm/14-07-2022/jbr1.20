public class Rectangle {
    float length = 1.0f;
    float width = 1.0f;
    public float getLength() {
        return length;
    }
    public void setLength(float length) {
        this.length = length;
    }
    public float getWidth() {
        return width;
    }
    public void setWidth(float width) {
        this.width = width;
    }
    public Rectangle(float length, float width) {
        this.length = length;
        this.width = width;
    }
    public Rectangle() {
        return;
    }
    public double getArea() {
        return length * width;
    }
    public double getPerimeter() {
        return (length + width) * 2;
    }

    @Override
    public String toString() {
        return "Rectangle [Dien tich: " + getArea() + ", Chu vi: " + getPerimeter() + "]";
    }
    
}
