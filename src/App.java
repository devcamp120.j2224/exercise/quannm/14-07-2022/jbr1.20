

public class App {
    public static void main(String[] args) throws Exception {
        System.out.println("Hello, World!");

        Rectangle rectangle1 = new Rectangle();
        Rectangle rectangle2 = new Rectangle(3.0f, 2.0f);

        System.out.println(rectangle1 + ", " + rectangle2);
    }
}
